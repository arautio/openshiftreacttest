const express = require('express');
const next = require('next');

const dev = process.env.NODE_ENV !== 'production';
const app = next({dir: './src/client', dev });
const handle = app.getRequestHandler();

var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || 'localhost'

app.prepare().then(() => {
  const server = express();

  server.get('*', (req, res) => {
    return handle(req, res);
  });

  /* eslint-disable no-console */
  server.listen(server_port,server_ip_address, (err) => {
    if (err) throw err;
    console.log( "Listening on http://" + server_ip_address + ":" + server_port )
  });
});